/*
 * HCSR04.c
 *
 *  Created on: Aug 22, 2021
 *      Author: Lion
 */

#include "HCSR04.h"

void HCSR04_Init(HCSR04_TypeDef *sonar, GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, TIM_HandleTypeDef *htim)
{
  sonar->GPIOx = GPIOx;
  sonar->GPIO_Pin = GPIO_Pin;
  sonar->associated_timer = htim;
}

void HCSR04_Start(HCSR04_TypeDef sonar)
{
  HAL_GPIO_WritePin(sonar.GPIOx, sonar.GPIO_Pin, SET);
  uint8_t delay = __HAL_TIM_GET_COUNTER(sonar.associated_timer) + 20;
  while (__HAL_TIM_GET_COUNTER(sonar.associated_timer) <= delay)
    ;
  HAL_GPIO_WritePin(sonar.GPIOx, sonar.GPIO_Pin, RESET);
}
