/*
 * MG996R.c
 *
 *  Created on: 26 ago 2021
 *      Author: Lion
 */

#include <MG995.h>
#include <utils.h>

#define FACTOR 1.1111 // Minimum step is 2 degree, only 1 sometimes skips the movement

static double convert(uint8_t degree)
{
	assert_param(degree >= 0 && degree <= 180);

	degree /= 2;
	return LOWER_LIMIT_PULSE + degree * FACTOR;
}

void MG995_Init(MG995_TypeDef *servo, TIM_HandleTypeDef *htim, uint32_t channel)
{
	servo->associated_timer = htim;
	servo->channel = channel;
}

void MG995_Step(MG995_TypeDef servo, uint8_t positive)
{
	double next_step;
	if (positive)
	{
		next_step = __HAL_TIM_GET_COMPARE(servo.associated_timer, servo.channel) + FACTOR;
		if (next_step > UPPER_LIMIT_PULSE)
			next_step = UPPER_LIMIT_PULSE;
	}
	else
	{
		next_step = __HAL_TIM_GET_COMPARE(servo.associated_timer, servo.channel) - FACTOR;
		if (next_step < LOWER_LIMIT_PULSE)
			next_step = LOWER_LIMIT_PULSE;
	}
	__HAL_TIM_SET_COMPARE(servo.associated_timer, servo.channel, next_step);
}

void MG995_Move(MG995_TypeDef servo, uint8_t position)
{
	// Positions go from 0 to 180 degrees, with a duty cycle from 0.5 to 2.5 ms
	if (position < 0)
		position = 0;
	else if (position > 180)
		position = 180;
	double pulse = convert(position);
	__HAL_TIM_SET_COMPARE(servo.associated_timer, servo.channel, pulse);
}

void MG995_Move_to_start(MG995_TypeDef servo)
{
	__HAL_TIM_SET_COMPARE(servo.associated_timer, servo.channel, LOWER_LIMIT_PULSE);
}

void MG995_Move_to_end(MG995_TypeDef servo)
{
	__HAL_TIM_SET_COMPARE(servo.associated_timer, servo.channel, UPPER_LIMIT_PULSE);
}

uint8_t MG995_Get_position(MG995_TypeDef servo)
{
	return ((__HAL_TIM_GET_COMPARE(servo.associated_timer, servo.channel) - LOWER_LIMIT_PULSE) / FACTOR) * 2;
}
