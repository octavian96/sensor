/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <MG995.h>
#include <HCSR04.h>
#include <utils.h>

#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define SWAP(x,y) { x+=y; y=x-y; x-=y; }
#define SORT(x,y) { x>y && (SWAP(x,y)); }

#define READS_NUM     5

#define PAN_LIMIT     UPPER_LIMIT
#define TILT_LIMIT    UPPER_LIMIT / 2
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
DMA_HandleTypeDef hdma_tim5_ch1;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;

/* USER CODE BEGIN PV */
volatile uint8_t command = SHUTDOWN;
const uint64_t CONTROL = 0xFFFFFFFFFFFFFFFF;
volatile uint8_t captureDone = FALSE;
uint32_t captures[2];
uint32_t measures[READS_NUM];
HCSR04_TypeDef sonar;
MG995_TypeDef h_servo, v_servo;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
static void loop_function(void);
static void scan_step(uint8_t);
static void radar_step(uint8_t);
static uint32_t median5(void);
static uint32_t measure(void);
static void send_coordinates(Position_TypeDef*);
static Position_TypeDef coordinates(uint32_t, MG995_TypeDef,
		MG995_TypeDef);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1) {
		captureDone = TRUE;
	}
}
/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TIM5_Init();
	MX_TIM4_Init();
	MX_USART2_UART_Init();
	/* USER CODE BEGIN 2 */
	// Init USS and servos
	HCSR04_Init(&sonar, GPIOA, GPIO_PIN_1, &htim5);
	MG995_Init(&h_servo, &htim4, TIM_CHANNEL_1);
	MG995_Init(&v_servo, &htim4, TIM_CHANNEL_2);

	// Enable UART
	__HAL_UART_ENABLE(&huart2);
	// Start DMA receive on UART
	HAL_UART_Receive_DMA(&huart2, (uint8_t*) &command, 1);

	// Start timer in Input Capture DMA mode
	HAL_TIM_IC_Start_DMA(&htim5, TIM_CHANNEL_1, captures, 2);
	// Start timers in PWM mode
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_2);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	scan_step(TRUE);
	HAL_Delay(500);
	while (1) {
		/* USER CODE END WHILE */
		HAL_GPIO_WritePin(GPIOD, BLUE_LED, RESET);
		HAL_GPIO_WritePin(GPIOD, GREEN_LED, RESET);
		HAL_GPIO_WritePin(GPIOD, ORANGE_LED, RESET);
		HAL_GPIO_WritePin(GPIOD, RED_LED, SET);
		HAL_SuspendTick();
		HAL_PWR_EnableSleepOnExit();

		HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);

		HAL_ResumeTick();
		HAL_GPIO_WritePin(GPIOD, RED_LED, RESET);

		loop_function();
		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV2;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief TIM4 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM4_Init(void) {

	/* USER CODE BEGIN TIM4_Init 0 */

	/* USER CODE END TIM4_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_OC_InitTypeDef sConfigOC = { 0 };

	/* USER CODE BEGIN TIM4_Init 1 */

	/* USER CODE END TIM4_Init 1 */
	htim4.Instance = TIM4;
	htim4.Init.Prescaler = 79;
	htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim4.Init.Period = 999;
	htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_PWM_Init(&htim4) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigOC.OCMode = TIM_OCMODE_PWM1;
	sConfigOC.Pulse = 0;
	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1)
			!= HAL_OK) {
		Error_Handler();
	}
	if (HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM4_Init 2 */

	/* USER CODE END TIM4_Init 2 */
	HAL_TIM_MspPostInit(&htim4);

}

/**
 * @brief TIM5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM5_Init(void) {

	/* USER CODE BEGIN TIM5_Init 0 */

	/* USER CODE END TIM5_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = { 0 };
	TIM_IC_InitTypeDef sConfigIC = { 0 };

	/* USER CODE BEGIN TIM5_Init 1 */

	/* USER CODE END TIM5_Init 1 */
	htim5.Instance = TIM5;
	htim5.Init.Prescaler = 3;
	htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim5.Init.Period = 999999;
	htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_IC_Init(&htim5) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_BOTHEDGE;
	sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
	sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
	sConfigIC.ICFilter = 0;
	if (HAL_TIM_IC_ConfigChannel(&htim5, &sConfigIC, TIM_CHANNEL_1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM5_Init 2 */

	/* USER CODE END TIM5_Init 2 */

}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void) {

	/* USER CODE BEGIN USART2_Init 0 */

	/* USER CODE END USART2_Init 0 */

	/* USER CODE BEGIN USART2_Init 1 */

	/* USER CODE END USART2_Init 1 */
	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN USART2_Init 2 */

	/* USER CODE END USART2_Init 2 */

}

/**
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) {

	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Stream2_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream2_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream2_IRQn);
	/* DMA1_Stream5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	/* USER CODE BEGIN MX_GPIO_Init_1 */
	/* USER CODE END MX_GPIO_Init_1 */

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, GPIO_PIN_RESET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD,
	GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15, GPIO_PIN_RESET);

	/*Configure GPIO pin : PA1 */
	GPIO_InitStruct.Pin = GPIO_PIN_1;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PD12 PD13 PD14 PD15 */
	GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/* USER CODE BEGIN MX_GPIO_Init_2 */
	/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */
static void loop_function(void) {
	Position_TypeDef position;
	uint32_t distance = 0;
	while (command != SHUTDOWN) {
		if (command & REPOSITION) { // Reset the servos position
			command ^= REPOSITION;
			if(command & RISE) command ^= RISE;
			if(command & DOWN) command ^= DOWN;
			//HAL_GPIO_TogglePin(GPIOD, WAITING_LED);
			HAL_GPIO_WritePin(GPIOD, SPHERICAL_LED, RESET);
			HAL_GPIO_WritePin(GPIOD, CARTESIAN_LED, RESET);
			scan_step(TRUE);
		}
		else if (command & RISE){
			command ^= RISE;
			if (command & DOWN){
				command ^= DOWN;
				MG995_Step(v_servo, FALSE);
			}
			else
				MG995_Step(v_servo, TRUE);
		}
		if (command & SEND) {
			//HAL_GPIO_WritePin(GPIOD, WAITING_LED, RESET);

			if (command & RADAR) {
				radar_step(FALSE);
			} else {
				scan_step(FALSE);
			}

			distance = measure();
			position = coordinates(distance, h_servo, v_servo);

			send_coordinates(&position);
		} else
			HAL_Delay(500);
	}
}
static void scan_step(uint8_t reset) {
	// TODO: Compare to a range of UPPER/LOWER_LIMIT, not to the exact value
	static uint8_t positive = TRUE, vertical = FALSE;

	if (reset) {
		positive = TRUE;
		vertical = FALSE;
		MG995_Move_to_start(h_servo);
		MG995_Move_to_start(v_servo);
	} else if (vertical) {
		MG995_Step(v_servo, TRUE);
		vertical = !vertical;
	} else {
		MG995_Step(h_servo, positive);
		uint8_t h_position = MG995_Get_position(h_servo);
		if (h_position >= PAN_LIMIT || h_position <= LOWER_LIMIT) {
			if (MG995_Get_position(v_servo) >= TILT_LIMIT) {
				command ^= SEND;
				positive = TRUE, vertical = FALSE;
			} else {
				vertical = TRUE;
				positive = !positive;
			}
		}
	}
	HAL_Delay(500);
}

static void radar_step(uint8_t reset) {
	// TODO: Compare to a range of UPPER/LOWER_LIMIT, not to the exact value
	static uint8_t positive = TRUE;

	if (reset) {
		positive = TRUE;
		if (MG995_Get_position(h_servo))
			MG995_Move_to_start(h_servo);
		if (MG995_Get_position(v_servo))
			MG995_Move_to_start(v_servo);
	} else {
		MG995_Step(h_servo, positive);
		uint8_t h_position = MG995_Get_position(h_servo);
		if (h_position == UPPER_LIMIT || h_position == LOWER_LIMIT)
			positive = !positive;
	}
	HAL_Delay(500);
}

static uint32_t median5(void) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-value"
	SORT(measures[0], measures[1]);
	SORT(measures[3], measures[4]);

	SORT(measures[0], measures[3]);
	SORT(measures[1], measures[4]);

	SORT(measures[1], measures[2]);
	SORT(measures[2], measures[3]);

	SORT(measures[1], measures[2]);
#pragma GCC diagnostic pop

	return (measures[2]);
}

static void send_coordinates(Position_TypeDef *position) {
	HAL_GPIO_WritePin(GPIOD, GREEN_LED, SET);
	HAL_UART_Transmit(&huart2, (uint8_t*) &(position->x), sizeof(position->x),
	HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart2, (uint8_t*) &(position->y), sizeof(position->y),
	HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart2, (uint8_t*) &(position->z), sizeof(position->z),
	HAL_MAX_DELAY);
	HAL_UART_Transmit(&huart2, (uint8_t*) &CONTROL, sizeof(CONTROL),
	HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOD, GREEN_LED, RESET);
}

static uint32_t measure(void) {
	uint32_t diffCapture = 0;
	for (unsigned char read = 0; read < READS_NUM; ++read) {

		__HAL_TIM_SET_COUNTER(&htim5, 0); // htim5->Instance->CNT = 0;
		HCSR04_Start(sonar);

		// Wait for the reading to be completed
		while (captureDone == FALSE)
			HAL_Delay(1);

		captureDone = FALSE;

		if (captures[1] >= captures[0])
			measures[read] = captures[1] - captures[0];
		else
			measures[read] = (__HAL_TIM_GET_AUTORELOAD(&htim5) - captures[0])
					+ captures[1];
	}

	// Since every timer tick should be a microsecond (Clock@4MHz, Prescaler@3, Period@999999),
	// there should not be the need to convert it in any way
	diffCapture = median5();
	//diffCapture = measures[0];
	diffCapture = diffCapture >> 1; //diffCapture /= 2;
	diffCapture = diffCapture * SOUNDSPEED;
	return diffCapture;
}

static Position_TypeDef coordinates(uint32_t distance,
		MG995_TypeDef h_servo, MG995_TypeDef v_servo) {
	Position_TypeDef coordinates;
	coordinates.x = MG995_Get_position(h_servo);
	coordinates.y = MG995_Get_position(v_servo);
	coordinates.z = distance;

	return coordinates;
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
