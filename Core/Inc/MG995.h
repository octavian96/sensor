/*
 * MG996R.h
 *
 *  Created on: 26 ago 2021
 *      Author: Lion
 */

#ifndef INC_MG995_H_
#define INC_MG995_H_

#define LOWER_LIMIT 0
#define UPPER_LIMIT 180
#define LOWER_LIMIT_PULSE 25
#define UPPER_LIMIT_PULSE 125

#include "stm32f4xx_hal.h"

typedef struct MG995_TypeDef
{
	TIM_HandleTypeDef *associated_timer;
	uint32_t channel;
} MG995_TypeDef;

void MG995_Init(MG995_TypeDef *, TIM_HandleTypeDef *, uint32_t);
void MG995_Step(MG995_TypeDef servo, uint8_t positive);
void MG995_Move(MG995_TypeDef, uint8_t);
void MG995_Move_to_start(MG995_TypeDef);
void MG995_Move_to_end(MG995_TypeDef);
uint8_t MG995_Get_position(MG995_TypeDef);

#endif /* INC_MG995_H_ */
