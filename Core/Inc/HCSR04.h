/*
 * HCSR04.h
 *
 *  Created on: Aug 22, 2021
 *      Author: Lion
 */

#ifndef SRC_HCSR04_H_
#define SRC_HCSR04_H_

#include "stm32f4xx_hal.h"

typedef struct HCSR04_TypeDef
{
	GPIO_TypeDef *GPIOx;
	uint16_t GPIO_Pin;
	TIM_HandleTypeDef *associated_timer;
} HCSR04_TypeDef;

void HCSR04_Init(HCSR04_TypeDef *, GPIO_TypeDef *, uint16_t, TIM_HandleTypeDef *);
void HCSR04_Start(HCSR04_TypeDef);

#endif /* SRC_HCSR04_H_ */
