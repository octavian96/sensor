/*
 * utils.h
 *
 *  Created on: Aug 30, 2021
 *      Author: Lion
 */

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

enum
{
	FALSE = 0U,
	TRUE = !FALSE
};

#define SOUNDSPEED 343

typedef struct Position_TypeDef
{
	uint_least8_t x;
	int_least8_t y;
	double z;
} Position_TypeDef;

#endif /* INC_UTILS_H_ */
